<?php
/**
 * PHPMailer multiple files upload and send
 */

//Import PHPMailer classes into the global namespace
//These must be at the top of your script, not inside a function
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

require "./PHPMailer/PHPMailer.php";
require "./PHPMailer/SMTP.php";
require "./PHPMailer/Exception.php";

// Recibir y decodificar los datos JSON
$data = json_decode(file_get_contents("php://input"), true);

// Validar que todos los campos requeridos estén presentes
if (
    empty($data["nombre"]) ||
    empty($data["telefono"]) ||
    empty($data["email"]) ||
    empty($data["mensaje"])
) {
    echo json_encode([
        "success" => false,
        "message" => "Por favor complete todos los campos requeridos.",
    ]);
    exit();
}

try {
    $mail = new PHPMailer(true);

    // Configuración del servidor
    $mail->isSMTP();
    $mail->Host = "smtp.hostinger.com";
    $mail->SMTPAuth = true;
    $mail->Username = "noreply@eter3d.com.mx";
    $mail->Password = "VMYsq@n8RMyr";
    $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;
    $mail->Port = 587;

    // Remitentes y destinatarios
    $mail->setFrom("noreply@eter3d.com.mx", "Formulario de Contacto");
    $mail->addAddress("ventas@eter3d.com.mx");
    $mail->addReplyTo($data["email"], $data["nombre"]);

    // Contenido
    $mail->isHTML(true);
    $mail->Subject = "Nuevo mensaje de contacto desde el sitio web";

    // Construir el cuerpo del correo
    $mensaje = "
    <h2>Nuevo mensaje de contacto</h2>
    <p><strong>Nombre:</strong> {$data["nombre"]}</p>
    <p><strong>Teléfono:</strong> {$data["telefono"]}</p>
    <p><strong>Email:</strong> {$data["email"]}</p>
    <p><strong>Empresa:</strong> {$data["empresa"]}</p>
    <p><strong>Mensaje:</strong></p>
    <p>{$data["mensaje"]}</p>
    ";

    $mail->Body = $mensaje;
    $mail->AltBody = strip_tags($mensaje);

    $mail->send();

    echo json_encode([
        "success" => true,
        "message" => "Mensaje enviado correctamente",
    ]);
} catch (Exception $e) {
    echo json_encode([
        "success" => false,
        "message" => "No se pudo enviar el mensaje. Error: {$mail->ErrorInfo}",
    ]);
}
