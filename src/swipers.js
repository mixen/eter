// core version + navigation, pagination modules:
import Swiper from 'swiper';
import { Autoplay, Navigation } from 'swiper/modules';
// import Swiper and modules styles
import 'swiper/css';
import 'swiper/css/navigation';

// init Swipers:
const swiperJumbotron = new Swiper('.swiper-jumbotron', {
    // configure Swiper to use modules
    modules: [
      Navigation,
      Autoplay
    ],

    autoplay: {
      delay: 30000,
      disableOnInteraction: false,
      reverseDirection: false,
    },

    // Optional parameters
    spaceBetween: 30,
    slidesPerView: 1,
    breakpoints: {
      576: {
        slidesPerView: 1,
      },
      768: {
        slidesPerView: 1,
      },
      992: {
        slidesPerView: 1,
      },
      1200: {
          slidesPerView: 1,
      },
    },

    // Navigation arrows
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },
});