import Swal from "sweetalert2";
import "sweetalert2/dist/sweetalert2.min.css";

document.addEventListener("DOMContentLoaded", function () {
    const form = document.getElementById("contactForm");

    form.addEventListener("submit", function (e) {
        e.preventDefault();

        if (!form.checkValidity()) {
            e.stopPropagation();
            form.classList.add("was-validated");
            return;
        }

        // Obtener los valores del formulario
        const formData = {
            nombre: document.getElementById("nombre").value,
            telefono: document.getElementById("telefono").value,
            email: document.getElementById("email").value,
            empresa: document.getElementById("empresa").value,
            mensaje: document.getElementById("mensaje").value,
        };

        // Deshabilitar el botón de envío para prevenir múltiples submissions
        const submitButton = form.querySelector('button[type="submit"]');
        submitButton.disabled = true;
        submitButton.innerHTML =
            '<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Enviando...';

        // Realizar la petición AJAX
        fetch("enviar-correo.php", {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify(formData),
        })
            .then((response) => response.json())
            .then((data) => {
                if (data.success) {
                    // Mostrar mensaje de éxito
                    Swal.fire({
                        title: "¡Mensaje enviado!",
                        text: "Gracias por contactarnos. En breve nos comunicaremos contigo.",
                        icon: "success",
                        showConfirmButton: false,
                        timer: 2000,
                    }).then(() => {
                        // Redireccionar a la página de gracias
                        window.location.href =
                            "https://www.eter3d.com.mx/gracias.html";
                    });
                } else {
                    // Mostrar mensaje de error
                    Swal.fire({
                        title: "Error",
                        text:
                            data.message ||
                            "Hubo un problema al enviar el mensaje. Por favor, intenta nuevamente.",
                        icon: "error",
                        confirmButtonText: "Ok",
                    });

                    // Reactivar el botón de envío
                    submitButton.disabled = false;
                    submitButton.innerHTML = "Enviar";
                }
            })
            .catch((error) => {
                console.error("Error:", error);
                Swal.fire({
                    title: "Error",
                    text: "Hubo un problema de conexión. Por favor, intenta nuevamente.",
                    icon: "error",
                    confirmButtonText: "Ok",
                });

                // Reactivar el botón de envío
                submitButton.disabled = false;
                submitButton.innerHTML = "Enviar";
            });
    });
});
