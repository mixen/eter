const webpack = require("webpack");
const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");

module.exports = {
    entry: "./src/app.js",
    output: {
        filename: "app.[contenthash].bundle.js",
        path: path.resolve(__dirname, "dist"), // <-- Output to 'dist' folder
    },
    mode: "production",
    module: {
        rules: [
            {
                test: /\.m?js$/,
                exclude: /node_modules/,
                use: {
                    loader: "babel-loader",
                    options: {
                        presets: ["@babel/preset-env"],
                    },
                },
            },
            {
                test: /\.css$/i,
                use: ["style-loader", "css-loader"],
            },
        ],
    },
    devServer: {
        static: {
            directory: path.join(__dirname, "/"),
        },
        compress: true,
        port: 7777,
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: path.resolve(__dirname, "src", "index.html"), // Path to your HTML template
            filename: path.resolve(__dirname, "", "index.html"), // Output HTML file
            inject: "body", // Inject scripts into the body tag
            minify: false, // Disable HTML minification
        }),
        new HtmlWebpackPlugin({
            template: path.resolve(__dirname, "src", "page-id.html"), // Path to your HTML template
            filename: path.resolve(__dirname, "", "page-id.html"), // Output HTML file
            inject: "body", // Inject scripts into the body tag
            minify: false, // Disable HTML minification
        }),
        new HtmlWebpackPlugin({
            template: path.resolve(__dirname, "src", "page-ma.html"), // Path to your HTML template
            filename: path.resolve(__dirname, "", "page-ma.html"), // Output HTML file
            inject: "body", // Inject scripts into the body tag
            minify: false, // Disable HTML minification
        }),
        new HtmlWebpackPlugin({
            template: path.resolve(__dirname, "src", "gracias.html"), // Path to your HTML template
            filename: path.resolve(__dirname, "", "gracias.html"), // Output HTML file
            inject: "body", // Inject scripts into the body tag
            minify: false, // Disable HTML minification
        }),
        new webpack.ProvidePlugin({
            $: "jquery",
            jQuery: "jquery",
        }),
    ],
};
